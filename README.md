# Negotiator

## Ruby version

ruby 2.6.3p62 (2019-04-16 revision 67580) [universal.x86_64-darwin19]

yarn 1.22.4

## System dependencies

* Developped on MAC OS
* `export RAILS_ENV=development`
* `export NODE_ENV=development`

## Configuration & installation

You need to run `bundle install --path vendor/bundle` to install all dependency.

`bundle exec rails yarn:install` to install all dependency.

## Database creation

`bundle exec rails db:migrate RAILS_ENV=development`.

## Database initialization

## How to run the test suite

## Services (job queues, cache servers, search engines, etc.)

## Deployment instructions

You can now run it with `bundle exec rails server`.

Add the same time, you need to run `bundle exec ./bin/webpack-dev-server`.
