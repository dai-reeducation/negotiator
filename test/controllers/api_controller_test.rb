# frozen_string_literal: true

require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest
  test 'should run api' do
    post api_index_url, params: { type: 'hosco', content: 'dGVzdA==' }

    assert_response :success
  end

  test 'should run api and return an error if parameter are missing' do
    post api_index_url, params: { type: '', content: 'dGVzdA==' }
    assert_response 400

    post api_index_url, params: { type: 'test', content: '' }
    assert_response 400

    post api_index_url
    assert_response 400

    post api_index_url, params: { type: '' }
    assert_response 400

    post api_index_url, params: { content: 'dGVzdA==' }
    assert_response 400
  end

  test 'should run api and return an error if content is not a valid base64' do
    post api_index_url, params: { type: 'test', content: '' }
    assert_response 400

    post api_index_url, params: { type: 'test' }
    assert_response 400
  end
end
