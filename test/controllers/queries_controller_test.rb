# frozen_string_literal: true

require 'test_helper'

class QueriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    post auth_login_url, params: { user: { email: 'test@test.com', password: 'test' } }
    @query = queries(:one)
  end

  test 'should get index' do
    get queries_url
    assert_response :success
  end

  test 'should show query' do
    get query_url(@query)
    assert_response :success
  end
end
