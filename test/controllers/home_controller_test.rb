# frozen_string_literal: true

require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test 'should redirect to root' do
    get queries_url
    assert_redirected_to root_url
  end

  test 'should display page if logged in' do
    post auth_login_url, params: { user: { email: 'test@test.com', password: 'test' } }
    get queries_url

    assert_response :success
  end
end
