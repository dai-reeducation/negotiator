# frozen_string_literal: true

require 'test_helper'

class AuthControllerTest < ActionDispatch::IntegrationTest
  test 'should get login' do
    get auth_login_url
    assert_response :success
  end

  test 'should get logout' do
    get auth_logout_url
    assert_redirected_to root_url
  end

  test 'should allow login' do
    post auth_login_url, params: { user: { email: 'test@test.com', password: 'test' } }
    assert_redirected_to root_url
  end

  test 'should not allow login' do
    post auth_login_url, params: { user: { email: 'test@test.com', password: 'test12345' } }
    assert_response :success
    assert_equal "Nom d'utilisateur et/ou mot de passe incorrect", flash[:error]

    post auth_login_url, params: { user: { email: 'test12345@test.com', password: 'test12345' } }
    assert_response :success
    assert_equal "Nom d'utilisateur et/ou mot de passe incorrect", flash[:error]
  end
end
