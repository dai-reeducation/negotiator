# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_05_204435) do

  create_table "queries", force: :cascade do |t|
    t.integer "request_log_id", null: false
    t.integer "response_log_id", null: false
    t.string "key"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["key"], name: "index_queries_on_key", unique: true
    t.index ["request_log_id"], name: "index_queries_on_request_log_id"
    t.index ["response_log_id"], name: "index_queries_on_response_log_id"
  end

  create_table "request_logs", force: :cascade do |t|
    t.string "fullpath"
    t.string "method"
    t.text "query_parameters"
    t.string "ip"
    t.string "remote_ip"
    t.text "body"
    t.integer "content_length"
    t.text "raw_post"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "response_logs", force: :cascade do |t|
    t.integer "code"
    t.text "body"
    t.text "headers"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "queries", "request_logs"
  add_foreign_key "queries", "response_logs"
end
