class CreateRequestLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :request_logs do |t|
      t.string :fullpath
      t.string :method
      t.text :query_parameters
      t.string :ip
      t.string :remote_ip
      t.text :body
      t.integer :content_length
      t.text :raw_post

      t.timestamps
    end
  end
end
