class CreateResponseLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :response_logs do |t|
      t.integer :code
      t.text :body
      t.text :headers

      t.timestamps
    end
  end
end
