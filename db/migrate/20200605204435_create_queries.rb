class CreateQueries < ActiveRecord::Migration[6.0]
  def change
    create_table :queries do |t|
      t.references :request_log, null: false, foreign_key: true
      t.references :response_log, null: false, foreign_key: true
      t.string :key

      t.timestamps
    end
    add_index :queries, :key, unique: true
  end
end
