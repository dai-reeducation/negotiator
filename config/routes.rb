Rails.application.routes.draw do
  root :to => 'home#index'
  resources :queries, only: [:index, :show]
  get '/', to: redirect('/auth/login')

  get '/auth/login'
  post '/auth/login'
  get '/auth/logout'

  post '/api/index'
end
