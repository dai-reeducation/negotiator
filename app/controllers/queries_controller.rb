# frozen_string_literal: true

# display all queries
class QueriesController < ApplicationController
  before_action :set_query, only: [:show]

  # GET /queries
  # GET /queries.json
  def index
    unless @current_user
      flash[:error] = 'Accès interdit'
      redirect_to root_url
      return
    end

    @queries = Query.order(created_at: :desc).page params[:page]
  end

  # GET /queries/1
  # GET /queries/1.json
  def show
    return if @current_user

    flash[:error] = 'Accès interdit'
    redirect_to root_url
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_query
    @query = Query.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def query_params
    params.fetch(:query, {})
  end
end
