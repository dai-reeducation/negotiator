# frozen_string_literal: true

# manage the authentification
class AuthController < ApplicationController
  def login
    return unless request.post?

    user = User.where(email: params[:user][:email]).first

    if user&.authenticate(params[:user][:password])
      save_logged_user(user)
    else
      flash[:error] = "Nom d'utilisateur et/ou mot de passe incorrect"
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to root_url
    flash[:info] = 'Déconnexion'
  end

  private

  def save_logged_user(user)
    session[:user_id] = user.id
    flash[:info] = "Bienvenue #{user.email} !"
    redirect_to root_url
  end
end
