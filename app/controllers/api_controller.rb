# frozen_string_literal: true

# POST /api/index to send an api
class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  # rubocop:disable Metrics/AbcSize
  def index
    return render plain: 'Type is empty', status: :bad_request if params[:type].to_s.empty?
    return render plain: 'Content is empty', status: :bad_request if params[:content].to_s.empty?

    begin
      content = Base64.decode64(params[:content])
      type = params[:type]
    rescue StandardError
      return render plain: 'Content is not a valid base64', status: :bad_request
    end

    query = process_query(type, content)
    render json: query, status: :created
  end
  # rubocop:enable Metrics/AbcSize

  private

  def create_query
    now = DateTime.current
    query = Query.new
    query.key = now.to_s(:number)
    query.save
    query
  end

  def process_query(type, content)
    query = create_query
    query.request_log_id = create_request(request).id
    query.save

    response = call_external_api(type, content)
    query.response_log_id = create_response(response).id
    query.save

    query
  end

  # rubocop:disable Metrics/MethodLength
  def create_request(request)
    requestlog = RequestLog.new
    requestlog.fullpath = request.fullpath
    requestlog.method = request.method
    requestlog.query_parameters = request.query_parameters
    requestlog.ip = request.ip
    requestlog.remote_ip = request.remote_ip
    requestlog.body = request.body.string
    requestlog.content_length = request.content_length
    requestlog.raw_post = request.raw_post
    requestlog.save

    requestlog
  end
  # rubocop:enable Metrics/MethodLength

  def create_response(response)
    responselog = ResponseLog.new
    responselog.code = response.code
    responselog.body = response.body
    responselog.headers = response
    responselog.save

    responselog
  end

  # rubocop:disable Metrics/MethodLength
  def call_external_api(type, content)
    if type == 'test'
      url = 'https://test.com'
      method = Net::HTTP::Get
    elsif type == 'hosco'
      url = 'https://www.hosco.com'
      method = Net::HTTP::Post
    else
      url = 'https://www.google.com'
      method = Net::HTTP::Get
    end

    url = URI.parse(url)
    req = method.new(url)
    req.body = content
    Net::HTTP.start(url.host, url.port) do |http|
      http.request(req)
    end
  end
  # rubocop:enable Metrics/MethodLength
end
