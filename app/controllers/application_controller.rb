# frozen_string_literal: true

# the basic controller for the whole application
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user

  private

  def set_current_user
    logger.debug "Session user id #{session[:user_id]}"

    return unless session[:user_id]

    @current_user = User.find(session[:user_id])
    logger.debug "Current user #{@current_user}"
  end
end
