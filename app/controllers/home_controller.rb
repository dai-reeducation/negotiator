# frozen_string_literal: true

# redirect the user if authenticated or not
class HomeController < ApplicationController
  def index
    if @current_user
      redirect_to queries_url
    else
      redirect_to auth_login_url
    end
  end
end
