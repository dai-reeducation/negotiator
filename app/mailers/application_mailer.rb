# frozen_string_literal: true

# defined by rails new .
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
