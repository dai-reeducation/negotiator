# frozen_string_literal: true

# a user that can use the application
class User < ApplicationRecord
  has_secure_password
  has_secure_password :recovery_password, validations: false
end
