# frozen_string_literal: true

# the query saved in the database
class Query < ApplicationRecord
  belongs_to :request_log
  belongs_to :response_log
end
